package com.sri.www;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class BookController {

  @Autowired
  BookRepository bookRepository;

  @GetMapping("/sayhello")
  public String sayHello(){
    return "hii all";
  }

  @GetMapping("/getbooks")
  public List<Book> getAllBooks() {
    List<Book> books= (List<Book>) bookRepository.findAll();
    return books;
  }
}
